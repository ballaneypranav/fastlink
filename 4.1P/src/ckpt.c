/*This  file contains a short program written by K. Shriram to do */
/* script-level checkpointing of the ILINK and LODSCORE program */
/* The checkpointing process is described in the paper: */
/* A. A. Schaffer, S. K. Gupta, K. Shriram, and R. W. Cottingham, Jr. */
/* Avoiding Recomputation in Linkage Analysis, */
/* Hum. Hered. 44(1994), pp. 225-237. */

#include <stdio.h>
#include <string.h>
#include "commondefs.h"
#include "checkpointdefs.h"

#define  ILINKRun		1
#define  LODSCORERun		2
#define  LINKMAPRun             3
#define  MLINKRun               4

void
main ( argCount , argList )
  int  argCount ;
  char  * argList [ ] ;
{
  FILE  * scriptCheckpoint ;
  char  * scriptCheckpointFilename ;
  int  scriptRun ;
  int  scriptSkip ;
  int  argNumber ;
  int  status ;
  int  runType ;

  if ( ( 1 == argCount ) )
  {
    printf ( "usage: %s [ ilink | lodscore | linkmap | mlink] scriptname\n" , argList [ 0 ] ) ;
    exit ( EXIT_FAILURE) ;
  }

  if ( 2 == argCount )
    runType = ILINKRun ;
  else
  {
    if ( 0 == strcasecmp ( argList [ 1 ] , "ilink" ) )
      runType = ILINKRun ;
    else if ( 0 == strcasecmp ( argList [ 1 ] , "lodscore" ) )
      runType = LODSCORERun ;
    else if ( 0 == strcasecmp ( argList [ 1 ] , "linkmap" ) )
      runType = LINKMAPRun ;
    else if ( 0 == strcasecmp ( argList [ 1 ] , "mlink" ) )
      runType = MLINKRun ;
    else
    {
      printf ( "usage: %s [ ilink | lodscore | linkmap | mlink ] scriptname\n" , argList[ 0 ] ) ;
      exit ( EXIT_FAILURE ) ;
    }
  }

  if (ILINKRun == runType)
    scriptCheckpointFilename =  ScriptILINKCheckpointFilename;
  else if (LODSCORERun == runType)
    scriptCheckpointFilename =  ScriptLODSCORECheckpointFilename;
  else if (LINKMAPRun == runType)
    scriptCheckpointFilename =  ScriptLINKMAPCheckpointFilename;
  else if (MLINKRun == runType)
    scriptCheckpointFilename =  ScriptMLINKCheckpointFilename;
  
  scriptCheckpoint = fopen ( scriptCheckpointFilename , "r+" ) ;
  if ( NULL == scriptCheckpoint )
  {
    scriptCheckpoint = fopen ( scriptCheckpointFilename , "w" ) ;
    fprintf ( scriptCheckpoint , "0 0\n" ) ;
  }
  else
  {
    fscanf ( scriptCheckpoint , "%d %d" , & scriptRun , & scriptSkip ) ;
    scriptSkip += scriptRun ;  scriptRun = 0 ;
    rewind ( scriptCheckpoint ) ;
    fprintf ( scriptCheckpoint , "%d %d\n" , scriptRun , scriptSkip ) ;
  }
  fclose ( scriptCheckpoint ) ;

  systemCallString [ 0 ] = '\0' ;
  argNumber = ( 2 == argCount ? 1 : 2 ) ;
  for ( /* do nothing */ ; argNumber < argCount ; argNumber ++ )
  {
    strcat ( systemCallString , argList [ argNumber ] ) ;
    strcat ( systemCallString , " " ) ;
  }
  status = ( int ) system ( systemCallString ) ;
  if ( EXIT_SUCCESS == status ) {
    unlink ( scriptCheckpointFilename ) ;
    if (ILINKRun == runType) {
      unlink( ScriptILINKFinalOut);
      unlink( ScriptILINKStreamOut);
    }
    else if (LODSCORERun == runType){
      unlink( ScriptLODSCOREFinalOut);
      unlink( ScriptLODSCOREStreamOut);
    }
    else if (LINKMAPRun == runType){
      unlink( ScriptLINKMAPFinalOut);
      unlink( ScriptLINKMAPStreamOut);
    }
    else if (MLINKRun == runType) {
      unlink( ScriptMLINKFinalOut);
      unlink( ScriptMLINKStreamOut);
    }
  }
}

