#ifndef  _LODEFS_H

#define  _LODEFS_H

/* This file contains some definitions for the faster version of */
/* the LODSCORE program described in the papers: */
/* R. W. Cottingham, Jr., R. M. Idury, and A. A. Schaffer, */
/* Faster Sequential Genetic Linkage Computations */
/* American Journal of Human Gentics, 53(1993), pp. 252-263. */
/* and A. A. Schaffer, S. K. Gupta, K. Shriram, and R. W. Cottingham, Jr. */
/* Avoiding Recomputation in Linkage Analysis, Human Heredity */
/* 44(1994), pp. 225-237. */

#ifndef _COMMONDEFS_H
#include <commondefs.h>
#endif

#define scale           2.0   /*SCALE FACTOR*/

#ifndef byfamily
#define byfamily        false
#endif

#define epsilon         1.0e-6

/* typedef struct phenotype {
  binset phenf;
  double x[maxtrait];
  boolean missing;
  int aff, liability;
} phenotype; */


 int startped[maxped], endped[maxped];
 int locuslist1[maxsys], locuslist2[maxsys];
 int ilocus, jlocus, j, holdmutsys, nlocus1, nlocus2, hitsys;
 double holdmutmale, holdmutfemale, holdftheta, holdmtheta, holdratio;
 locusvalues *holdlocus[maxsys];

/*OTHERS*/
 int  nlocus, iplace, jplace;
 boolean inconsistent;
 FILE *pedfile, *recfile;
/*ILINK*/
 boolean penalty;
 double penlike;

/*GEMINI*/
enum {
  go, quit, restart
} gemini_continue_;

#if defined(DOS)
typedef enum
{
  normalRun ,
  checkpointedRun
}
checkpointType;

checkpointType checkpointStatus;
#endif
#endif  /* _LODEFS_H */
