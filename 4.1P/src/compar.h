/* This file contains common parallel routine and variable
   declarations for use with the parallel versions of the FASTLINK
   programs ILINK, LINKMAP, and MLINK. Sequential FASTLINK is an
   improved version of LINKAGE.  Improvements are described in
   described in: R. W. Cottingham, Jr., R. M. Idury, and
   A. A. Schaffer Faster Sequential Genetic Linkage Computations
   American Journal of Human Genetics, 53(1993), pp. 252--263 and
   A. A. Schaffer, S. K. Gupta, K. Shriram, and R. W. Cottingham, Jr.,
   Avoiding Recomputation in Linkage Analysis Human Heredity 44(1994),
   pp. 225-237.  The parallel implementations of ILINK are described in:
   S. Dwarkadas, A. A. Schaffer, R. W. Cottingham Jr., A. L. Cox,
   P. Keleher, and W. Zwaenepoel, Parallelization of General Linkage
   Analysis Problems, Human Heredity 44(1994), pp. 127-141 and
   S. K. Gupta, A. A. Schaffer, A. L. Cox, S. Dwarkadas, and
   W. Zwaenepoel, Integerating Parallelization Strategies for Linkage
   Analysis, Computers and Biomedical Research, to appear.
   The code in this file was written by Chris Hyams. */

#if !defined(COMPAR_H)
#define COMPAR_H

/* index into executionTimes array */
int eTimeIndex;

/* Shared memory allocation macros */
#define oneMeg (1 << 20)
#define tenMegs (10 * oneMeg)

/* When allocating shared memory, we determine at run-time a good
   approximation of the total number of bytes we will request using
   Tmk_malloc().  Then, we add a fudge factor to take into account
   other run-time allocations, like the string buffers, etc.  The
   fudge factor is either some percentage of the total usage
   calculated, or the maximum we allow -- whichever is smaller.  Next,
   we make sure to allocate at least the minimum we allow. */
#define minMemNeeded oneMeg       /* minimum shared memory we allocate */
#define maxMemFudge tenMegs       /* maximum fudge factor */
#define memFudgePercentage 0.25   /* fudge percentage */

#if !defined(ILINK)
/* Assign thetas, etc.  Function is called only by processor 0.
   This function is analagous to gemini() in ILINK */
void iterpedControl();
#endif   /* !defined(ILINK) */

/* parallel startup/initialization code */
#if !defined(KNR_PROTO)
void parStartup(int argc, char** argv);
#else
void parStartup();
#endif  /* !defined(KNR_PROTO) */

/* parallel ending/cleanup code */
void parFinish();

/* Parallel library startup code */
#if !defined(KNR_PROTO)
void initParLib(int argc, char** argv);
#else
void initParLib();
#endif  /* !defined(KNR_PROTO) */

/* Allocate and initialize barriers, and allocate global memory */
void gMemAndBarrierAlloc();

/* procedure to manage child processes in parallel computation */
void child();

/* iterates over processes, creating children */
void childLoop();

/* Set up data structures for parallel theta evaluations --
   used to be SandeepSetup() */
void parThetaSetup();

/* Called after inputdata() to check to see if there are any
   constants set that provide situations we have not yet
   implemented */
void checkNotImplemented();

#if !defined(KNR_PROTO)
void printBarrier(char*, int);
#else
void printBarrier();
#endif  /* !defined(KNR_PROTO) */

#if defined(ILINK)
void outf();
void gemini();
void gcentral();
void gforward();
#endif  /* defined(ILINK) */

#endif  /* if !defined(COMPAR_H) */
