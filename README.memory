From: ftp-bimas.cit.nih.gov                       Last mod: June 27, 1999

                  FASTLINK, version 3.0P and beyond


This file discusses memory requirements for FASTLINK. See the top level
README file for a roadmap to all FASTLINK documentation.

|*| Memory Requirements
    -------------------

The FASTLINK programs can require large amounts of memory when doing
multilocus analysis.  Of course the amount of memory required is very
dependent on the number of loci and the number of alleles at each
locus.  However even 100 Mb is not a problem to run under Sun OS for
instance, because this is a virtual memory operating system.  Ideally
one would want to run a program of this size on a machine with 32 Mb
of memory, but in our experience it is possible to run on machines
with as little as 12 Mb.

Of course it is necessary to have a swap file with sufficient space to
run the OS and have enough free space for the program.

To see how much space a program requires in version 2.0
or earlier, it was possible to use the unix command:

	size <program name>

for instance using linkmap from FASTLINK, 2.0:

	unix> /usr/bin/size linkmap

	text    data    bss     	dec     	hex
        139264	8192	28719480	28866936	1b87978

This value under "dec" is the decimal number of bytes for the whole
program.  So we see in this case that 28.9 Mbytes is required.

Then compare this with the unix pstat command:

	unix> /etc/pstat -s
	14880k allocated + 3712k reserved = 18592k used, 169000k available

This indicates that a total of 187592 Kbytes or 187 Mb has been
allocated on this system for swap space, and with the current job mix,
18.5 Mb are used and 169 Mb are available.  So in this case linkmap
will be able to run.

To enlarge the swap space consult your local system administrator.
For a single user system running FASTLINK we recommend 150Mb total swap
space as a minimum.

Alternatively, use the "slow" versions of the programs.  The term slow
is a little misleading in that these versions will still be
significantly faster than the originals.  In the case of linkmap, the
version compiled with 
  make slowlinkmap
with the current constant settings is less than 1 Mb in size.  Any
unix system should have a swap file large enough for this.

Starting in version 2.1 of FASTLINK a lot of the memory allocation is
done dynamically (See README.updates).  In version 2.2 and beyond,
almost all the large data structures are allocated dynamically.  This
means that you will not be able to detect before running whether you
have enough memory.  If you do not have enough, the program should
exit politely with an explanation, shortly after startup. The
advantage of doing the memory allocation at runtime is that it may be
possible to use significantly less memory based on knowledge of
certain parameters that are available only at runtime.
