From: ftp-bimas.cit.nih.gov                       Last mod: July 13, 1995

                  FASTLINK, version 3.0P and beyond

How long will a sequential FASTLINK run take? This turns out to be
extremely difficult to estimate ahead of time, but relatively easy to
estimate once the run is underway.

Each FASTLINK run evaluates the same likelihood function at different
candidate thetas vector inputs.  For MLINK and LINKMAP the user specifies
all the candidate theta vectors.  For ILINK and LODSCORE they are generated
on the fly.  It is reasonably safe to assume that each candidate theta
takes roughly the same time to evaluate. Therefore, if you know how many
candidate thetas there will be, you multiply the number of thetas times the
running time for one theta.

Caution: This approach will not work on a computer where the load
(from other users) is varying significantly during the run.

You can estimate the time for one theta by watching the screen.  When the
first output gets printed after the header information, one theta is
complete. Alternatively, MLINK and LINKMAP take a checkpoint after every
theta. Therefore, by comparing the timestamps of the files
checkpoint.LINKMAP and checkpoint.LINKMAP.bak (or checkpoint.MLINK and
checkpoint.MLINK.bak), you may infer how long one candidate theta takes to
evaluate.  ILINK and LODSCORE usually take checkpoints every one or two
thetas, so you must be more careful in making inferences from the
timestamps of those checkpoint files.  The timestamp of a file can usually
be determined with the command "ls -l".

The number of thetas for ILINK and LODSCORE cannot be predetermined, but a
good estimate is (10 * number of loci) if you have sex-averaged thetas.  If
male theta and female theta differ, estimate with (20 * number of loci).
After each iteration, ILINK and LODSCORE print an update in which the
number following the string NFE (number of function evaluations) is the
number of candidate thetas already evaluated. See README.ILINK for more
details. These NFE numbers can be used to estimate how much more work
remains to be done by using the formula:

    (((Number of thetas estimated) / (Number of thetas completed)) 
    * (running time so far)) - (running time so far)

