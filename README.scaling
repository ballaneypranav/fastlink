From: ftp-bimas.cit.nih.gov                       Last mod: June 27, 1999

                  FASTLINK, version 4.0P

|*| Output values from FASTLINK are scaled
    --------------------------------------

The output log likelihood values printed by both LINKAGE
and FASTLINK are scaled on some pedigrees by an additive
constant that depends on the pedigree structure and selection
of loop breakers, if any. This means that output log likelihood
values should be used only by subtracting one from another
to obtain a LOD score. This problem first surfaced in the
initial release of FASTLINK because FASTLINK/LINKMAP uses
a different scaling convention from LINKAGE/LINKMAP.
See the next section below.

The scaling issue became fundamental with FASTLINK 4.0P
where the change in loop breaker choice means that the raw
output is unlikely to match earlier versions on looped pedigrees.
The reason is that when FASTLINK 4.0P changes the selection of
loop breakers, this has the side effect of changing the
scaling constant. Therefore, FASTLINK 4.0P can be compared
for correctness to earlier versions only by comparing LOD scores.

More changes in both loop breaker selection and genotype inference
for looped pedigrees were made in version 4.1P. So the printed
log likelihood values for versions 4.0P and 4.1P may differ
on looped pedigrees. In general, the value for 4.1P should be the
same or smaller in magnitude, indicating that less time
is being wasted exploring unnecessary genoype combinations
for the loop breakers.

|*| Scaling discrepancy - IMPORTANT FOR LINKMAP USERS
    --------------------

Prof. Ellen Wijsman (U. Washington) brought to our attention a
situation in which FASTLINK versions of LINKMAP print out some values
that differ from those printed out by LINKMAP in LINKAGE 5.1. What
follows are two explanations for the discrepancy, one short, and one
long.

Short Explanation. The different values represent differences in scaling
LINKMAP's representation of the likelihood value. If you run the post-processor
program which computes odds, the discrepancies will disappear.

Long Explanation. Because LINKAGE computes with very small numbers, these
numbers must be scaled to avoid underflow. Any (log) likelihood values that
are printed out by any of the LINKAGE programs are actually scaled by
some amount that depends on the structure of the input pedigree(s).
Various scaling rules can be used. In LINKAGE 5.1, the programs LODSCORE,
ILINK, and MLINK all use the same scaling rules, while LINKMAP uses
different scaling rules. We could find no internal or external documentation
to explain this difference. The difference arises only for some pedigrees
that have loops.

To increase the amount of code that the four programs can share in our
versions, we have decided to make our LINKMAP use the same scaling rules
as the other three programs. 

If you would like details on how to modify our LINKMAP to make it
consistent with the old LINKMAP contact schaffer@cs.rice.edu.
The necessary editing is simple, but you would have to edit the code each
time you switch between LINKMAP and one of the other three programs.
