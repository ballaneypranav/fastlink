From: ftp-bimas.cit.nih.gov                      Last mod: June 28, 1999

     Using Conditional Allele Frequencies or 
    Parameterized Disequiliobrium in FASTLINK
      By Ken Morgan and Alejandro Schaffer


Starting with FASTLINK 4.1, I am exploring the possibility of adding
several options to model linkage disequilibrium in FASTLINK.  On such
option, conditional allele frequencies, has been completed.
Conditional allele frequencies means that allele frequencies at
markers can depend on the genotype at the disease locus on the same
chromosome strand (haplotype). This is slightly more general and
flexible than the disequilibrium option currently allowed in
LINKAGE/FASTLINK

|*| Baiscs of using conditional allele frequencies
The user is assumed to be familiar with FASTLINK usage and estimating 
allele frequencies with ILINK.

The constant ALLELE_SPEED works as in FASTLINK. 
It is located in unknown.h and commondefs.h.
Set
 #define ALLELE_SPEED 1
to achieve greater speed in non-estimation mode, when using
conditional allele frequencies.

Set
 #define ALLELE_SPEED 0
 #define fitmodel true
to allow for estimation of frequencies.

 #define ALLELE_SPEED 0
 #define fitmodel true
is always safe but may be unnecessarily slow.

"Conditional allele frequences" means that at a marker, the frequency
of an allele may depend conditionally on the allele at the disease
locus. E.g., the relative frequencies of the haplotypes

     Disease   1     1
     Marker    1     2

     Disease   2     2
     Marker    1     2

may be quite different. 
Estimating conditional allele frequencies is conceptually different from
estimating haplotype frequencies because in the former case the
disease allele frequencies stay fixed, while in the latter case they do not
stay fixed.

To get started it is necessary to make a basic change in the format
of datafile.dat.
For each marker locus, put 2 lines of allele frequencies instead of 1.

E.g., instead of:

 3 5
 0.07000000 0.01000000 0.15000000 0.04000000 0.73000000

put

3 5
 0.07000000 0.01000000 0.15000000 0.04000000 0.73000000
 0.07000000 0.01000000 0.15000000 0.04000000 0.73000000

So long as the two lines of frequencies are equal and you are not estimating
frequencies, the results should be identical to regular FASTLINK.
If you want to use conditional allele frequencies, you must similarly
double all marker allele frequency lines in datafile.dat.
This is a reasonable requirement because if for some marker you do not
want the allele frequencies to vary depending on the disease allele,
then the two lines can be identical.

Do not change the way in which the disease locus is specified.

To tell unknown that you are using conditional allele frequencies, use
  unknown -c
instead of
  unknown

To tell mlink/ilink/linkmap that you are using conditional allele frequencies,
use:
  mlink -c
  ilink -c
  linkmap -c

instead of

  mlink
  ilink
  linkmap


To estimate conditional allele frequencies with ILINK, the procedure
is similar to regular ILINK.
At the bottom of datafile.dat are two lines that look like:

k
1 1 1 1 ...

or

k
0 1 1 1 ...

where k is index of the locus for which frequencies are to be
estimated, and
the first number of the last line is:
  0 if theta stays fixed
  1 if theta is to be estimated

the remainder of the last line has (a - 1) 1's where a is the number
of alleles at locus k.
Caution: The highest numbered allele, a, must occur or regular ILINK
will crash.

For conditional estimation, the last line of datafile.dat
should have 2a -1 numbers instead of a numbers.
Again the first number may be 0 or 1, and the remaining 2a-2
numbers should be 1's.

When ilink -c is used to estimate frequencies conditionally,
part of the output final.dat might look like:

GENE FREQUENCIES :
 0.309689 0.424004 0.250168 0.016138
CONDITIONAL (on disease allele) GENE FREQUENCIES :
 0.242056 0.303824 0.452248 0.001871

The first line is conditional on the healthy allele at the
disease locus. The second line is conditional on the unhealthy
allele at the disease locus.

|*| Usage Suggestions


1. To caluculate LOD scores for linkage under each of the 
   Linkage Disequilibrium (LD) (usingILINK -c) and linkage 
   equilibrium (LE; using ILINK) models requires  two analyses:

   (a) the log-likelihood for estimated marker allele frequencies and
   the recombination fraction (theta) between the marker and disease
   loci; 
   (b) the log-likelihood for estimated marker allele frequencies
   and fixed theta=0.5; the difference in the log-likelihoods is converted
   to a lod score.

2. For the test of LD,  compare the change in the
   log-likehoods under LD and LE where both theta and the marker allele
   frequencies are jointly estimated.
   One may assume that twice this difference is asymptotically distributed as
   chi-square with k -1 degrees of freedom (where k = number of
   distinct alleles of the marker locus in the data).  (The P-value may
   need to be estimated empirically for small samples for the situation
   where one or more conditional allele frequencies are estimated to be
   0.)

3. For a test of linkage allowing for linkage disequilibrium, there are
   tqo approaches.
   Approach A: Constrain the two vectors of conditional allele 
   frequencies to be equal when  theta=0.5.  
   Approach B: Allow the vectors of allele frequencies to be unconstrained
   in both cases. Then in the likelihood ratio test for
   linkage, the allele frequencies (conditional or not) become nuisance
   paramaters, and there is only 1 degree of freedom.
   Approach B (as a special case of the general idea of
   likelihood ratio tests with nuisance parameters)
   is advocated by Joe Terwilliger.


