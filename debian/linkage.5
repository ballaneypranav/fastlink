.TH LINKAGE 5 "April 22, 2003"
.SH NAME
LINKAGE \- general information about LINKAGE's file formats
.SH "DESCRIPTION"
The input data consist of pedigree and genotypic information contained
in one file, and locus descriptions, recombination rates and locus
order contained in a second file. These files are entitled
.BR PEDFILE (5)
and
.BR DATAFILE (5),
respectivly.
.SS "Phenotypes and Genotypes"
To understand the format of the input files, you must know what kinds
of phenotypic data can be interpreted by the LINKAGE
programs. Phenotype data can be one of the following types:
.IP \(bu 5
.BR "Numbered alleles".
These are codominant alleles at a single locus. The numbers run consecutively from 1 to the maximum number of alleles observed. The phenotype consists of two allele numbers corresponding to a genotype. An unknown genotype is coded as 0 0.
.IP \(bu 5
.BR "Binary factors".
In this coding scheme a series of binary codes (1 or 0) indicates the presence or absence of a phenotype factor. This system is useful for describing either codominant or recessive/dominant systems. The phenotype is entered as a binary string.
.IP \(bu 5
.BR "Affection status".
The presence or absence of disease (or other qualitative phenotypes) is described by a numbered code. A risk or liability class can also be included as a separate numeric code.
.IP \(bu 5
.BR "Quantitative traits".
One or more quantitative measurements can be used as a phenotype description.
.PP
The phenotypic codes for each of these types of data are described in more detail below. 
.TP
.BR "NOTE":
The present version of LINKAGE does not allow mixtures of affection status and quantitative variables, except in the case of sex-linked traits as described below. A future version will incorporate a modification allowing such mixtures for autosomal data. 
.SS "Numbered Allies"
Numbering alleles is the simplest way to code codominant marker
data. A homozygote is indicated by repeating the allele number; thus 1
2 indicates that the alleles are 1 and 2 (a heterozygotea) while 1 1
indicates the alleles are 1 and 1 (a homozygote). An unknown genotype
is coded as 0 0. For sex-linked loci, males have a single allele. With
the allele 1, for example, the phenotype can be coded 1 0 or 1 1.
.SS "Binary Factors"
Binary factors (sometimes called "factor-union" notation) can also
represent phenotypes for codominant marker data, but this coding is
most useful with recessive alleles or with complex systems such as Rh,
ABO, and Gm. Each allele is assigned a set of properties, called
factors, in such a way that all phenotypes can be specified as the
union of two allele sets.

For codominant loci, each allele can be associated with one factor. If
n alleles are present, the ith allele is represented by a series of n
binary codes with a 0 in all locations, except in the ith position,
which contains a 1. For example, in a two allele system the allelic
codes are:

.TS
center;
n1 n1 l.
1	0	(allele 1)
0	1	(allele 2)
.TE


The three possible phenotypes are:

.TS
center;
n1 n1 l.
1	0	(union of alleles 1 and 1)
1	1	(union of alleles 1 and 2)
0	1	(union of alleles 2 and 2)
.TE


An unknown phenotype is coded as 0 0. Spaces between the codes are
very important; they must be included when entering the phenotypes
into the pedigree file as described below.

A locus with three codominant alleles is coded as:

.TS
center;
n1 n1 n1 l.
1	0	0	(allele 1)
0	1	0	(allele 2)
0	0	1	(allele 3)
.TE


The six possible phenotypes are:

.TS
center;
n1 n1 n1 l.
1	0	0	(union of alleles 1 and 1)
1	1	0	(union of alleles 1 and 2)
1	0	1	(union of alleles 1 and 3)
0	1	0	(union of alleles 2 and 2)
0	1	1	(union of alleles 2 and 3)
0	0	1	(union of alleles 3 and 3)
.TE


and an unknown phenotype is 0 0 0.

The advantage of the binary factor coding scheme is evident when a
recessive disease gene is under study. To code such a system, we could
indicate the normal gene by the presence of a single factor (1) and
the disease gene by the absence of this factor (0). The phenotype 1
(unaffected) now corresponds to two possible genotypes, either the
union of allele 1 and allele 1 (noncarrier) or the union of allele 1
and allele 0 (carrier).

This simple coding is usually not sufficient because both homozygote
recessive and unknown phenotypes are coded as 0. To account for this,
we introduce a second factor for which a 1 indicates that the
phenotype is known, and a 0 that the phenotype is unknown. The allelic
codes are:

.TS
center;
n1 n1 l.
1	1	(allele 1)
0	1	(allele 2)
.TE

and the possible phenotypes are:

.TS
center;
n1 n1 l.
1	1	(union of alleles 1 and 1, or alleles 1 and 2)
0	1	(union of alleles 2 and 2)
0	0	(unknown)
.TE

.SS "Affected Status"
"Affection status" refers to the presence or absence of disease. The
programs assume that an affected individual will have the phenotype
code "2" and that an unknown individual will have the code "0." By
convention, "1" is used to designate unaffected status (in fact, this
code can be any integer value other than 0 and 2). If necessary, the
unknown and affected codes can be changed in the program code, and the
programs recompiled.

For an "affection-status" locus, each genotype has an associated
penetrance; this is the probability that an individual with a
particular genotype will be affected. Penetrance can also be defined
as a function of liability classes. In this case, one penetrance is
given for each genotype in each liability class. The classes are
numbered sequentially starting from 1. With two or more liability
classes, the phenotype is the affection status plus the class
number. When a single affection status class is defined, the class
number is not included as part of the phenotype.

With sex-linked traits, different penetrances must be given for
females and males. One penetrance in males is specified for each
allele in each liability class.
.SS "Quantitative Variables"
Phenotypic information is sometimes presented in the form of
quantitative measurements, e.g. creatine kinase for carrier detection
in Duchenne muscular dystrophy. The phenotype is then the quantitative
value. Unknown phenotypes are entered as 0.0. (The code for unknown
quantitative values is a program constant that can be changed.) The
genotypic means, the variance of the trait in homozygotes, and the
ratio of the variances in heterozygotes and homozygotes must be
specified.

If several traits are measured for the same locus, the phenotype is
the list of all the variables. A single value of 0.0 in the list is
interpreted as an unknown phenotype. The means must be given for each
variable as a function of genotype, along with the variance-covariance
matrix. The variance matrices for homozygotes and heterozygotes can
differ by a constant factor.

For sex-linked traits it is assumed that males will have an
affection-status variable rather than a quantitative value. If several
variables have been measured (ntrait), a male phenotype consists of
affection status followed by ntrait-1 arbitrary entries (for example,
zeros). The present version of the programs supports only one
affection status class, with full penetrance of the disease allele for
sex-linked traits.

.SH NOTES
The information contained herein was gleaned, often-times verbatim,
from 
.UR http://linkage.rockefeller.edu/soft/linkage/
the LINKAGE User's Guide
.UE
on the web by kind permission of Jurg Ott, Ph.D.

.SH AUTHORS
Mark Lathrop and Jurg Ott.
.PP
This manual page was written by Elizabeth Barham
<lizzy@soggytrousers.net> for the Debian GNU/Linux distribution.

.SH WORLD-WIDE-WEB
.UR http://linkage.rockefeller.edu/soft/linkage/
http://linkage.rockefeller.edu/soft/linkage/
.UE

.SH SEE ALSO
.BR DATAFILE (5),
.BR PEDFILE (5),
.BR ilink (1),
.BR linkmap (1),
.BR lodscore (1),
.BR mlink (1),
and
.BR unknown (1).

