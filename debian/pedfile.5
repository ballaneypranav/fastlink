.TH PEDFILE 5 "April 22, 2003"
.SH NAME
PEDFILE \- LINKAGE's PEDFILE, Pedigree Information
.SH DESCRIPTION
In addition to phenotypes and description of loci, the LINKAGE
programs require pedigree information in order to traverse the
pedigree when calculating the likelihood. The input must contain the
following information for each individual:
.IP \(bu 5
a pedigree number
.IP \(bu 5
an individual identification number, or id
.IP \(bu 5
father's id number
.IP \(bu 5
mother's id number
.IP \(bu 5
first offspring id number
.IP \(bu 5
next paternal sibling id number
.IP \(bu 5
next maternal sibling id number
.IP \(bu 5
sex
.IP \(bu 5
"proband status"
.PP
The first offspring can be any of an individual's children, but the
next sib id's for the offspring will be constrained by this
choice. The next-paternal-sibling and next-maternal-sibling numbers,
along with first-offspring number, provide a set of pointers to pass
from one child to the next. The first offspring of the father is any
of his children; the next paternal sib of the first offspring is any
other of his children, etc. The entry for the next paternal sib of the
last child is "0". Similar pointers are made for the mother's
children. For full-sibs, it is convenient to make id's for the next
maternal and next paternal siblings identical, but when one or both
parents have children from different marriages, at least some will
have different values.

Father and mother id's are 0 for founders, or other members of the
pedigree for whom information on parents is absent. Otherwise, both
parents must be present in the pedigree even if one is unknown. If one
parent is unknown, an id number must still be created, and a record
for the fictitious parent must appear in the pedigree file.

The "proband" refers to a starting individual for linkage calculations
(indicated by a 1 in the proband field). The choice of the proband is
not necessarily related to the ascertainment of the pedigree; indeed,
it is usually more efficient to calculate from a founding ancestor
rather than from the true proband. Risks are also calculated for the
person designated as the proband. Other individuals should have a 0 in
the proband field, except in pedigrees containing inbreeding or
marriage loops as discussed in section 2.8. If no proband is
designated, the first individual encountered for a pedigree will be
used as a starting point for the calculation.

The sex field is coded 1 for males and 2 for females. These default
values can be changed by modifying a program constant.

The creation of the first offspring, next maternal sib and next
paternal sib pointers is done automatically by the program MAKEPED
(not covered in this document). The input to the MAKEPED program is a
file with individual records containing the pedigree number, id
number, father id, mother id, sex and phenotypic data. The phenotypic
data are coded as discussed in
.BR LINKAGE (5)
and in the following examples. The MAKEPED program also allows
automatic selection of probands for efficient likelihood calculations.

.SH EXAMPLE
Consider the pedigree shown in Figure 1. Data on three loci are
presented: one disease locus and two marker loci. An "a" below an
individual stands for "affected," and "u" stands for unknown. The
first marker locus has three alleles present in the pedigree, while
the second has two alleles present.

                       [1]--.--(2)
                        a   |
                        22  |   12
                        12  |   12
         Figure 1           |
                     .------+------.
                     |      |      |
            (6)--.--[3]    [4]    (5)
                 |   a
             13  |   12
             12  |   22
                 |
             .-------.
             |       |
            [7]     (8)--.--(10)
             a           |
             13      u   |   12
             22      u   |   12
                         |
                        (9)
                         a
                         11
                         12

The input PEDFILE can take the following form:

.TS
center;
n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 l l.
1	1	0	0	3	0	0	1	1	2	0	1	0	1	1	Ped: 1 Per: 1
1	2	0	0	3	0	0	2	0	1	1	1	0	1	1	Ped: 1 Per: 2
1	3	1	2	7	4	4	1	0	2	1	1	0	0	1	Ped: 1 Per: 3
1	4	1	2	0	5	5	1	0	1	0	1	0	1	1	Ped: 1 Per: 4
1	5	1	2	0	0	0	2	0	2	1	1	0	0	1	Ped: 1 Per: 5
1	6	0	0	7	0	0	2	0	1	1	0	1	1	1	Ped: 1 Per: 6
1	7	3	6	0	8	8	1	0	2	1	0	1	0	1	Ped: 1 Per: 7
1	8	3	6	9	0	0	1	0	1	0	0	0	0	0	Ped: 1 Per: 8
1	9	8	10	0	0	0	2	0	2	1	0	0	1	1	Ped: 1 Per: 9
1	10	0	0	9	0	0	2	0	1	1	1	0	1	1	Ped: 1 Per: 10
.TE

The PEDFILE has been produced from an input file by the MAKEPED
program. The comments at the end of each record indicate original
pedigree and id codes. The first entry in each record is followed by
the pedigree number, id number, five pedigree pointers (father id,
mother id, first offspring id, next paternal sib id, next maternal sib
id), sex, proband, disease status, and marker loci coded as binary
factors. Comparison with the original pedigree will reveal the coding
scheme. Individual 1 has been chosen as the "proband;" as this is the
first individual of this pedigree encountered in the file, the entry
in the proband field is optional.

Now consider the same pedigree extended to include some half sibs
(Figure 2).

                                  [1]--.--(2)
                                   a   |
          Figure 2                 22  |   12
                                   12  |   12
                                       |
                                .------+------.
                                |      |      |
                       (6)--.--[3]    [4]    (5)
                            |   a             a
                        13  |   12     22     12
                        12  |   22     12     22
                            |
               .-------------------------.
               |                         |
     (11)--.--[7]--.--(12)--.--[13]     (8)--.--(10)
           |   a   |        |                |
      u    |   13  |   12   |   u        u   |   12
      u    |   22  |   12   |   u        u   |   12
           |       |        |                |
          (14)    (15)     (16)             (9)
                                             a
           13      13       11               11
           12      12       12               12

The MAKEPED program produces the following file for this pedigree (figure 2):

.TS
center;
n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 l l.
1	1	0	0	3	0	0	1	1	2	0	1	0	1	1	Ped: 1	Per: 1
1	2	0	0	3	0	0	2	0	1	1	1	0	1	1	Ped: 1	Per: 2
1	3	1	2	7	4	4	1	0	2	1	1	0	0	1	Ped: 1	Per: 3
1	4	1	2	0	5	5	1	0	1	0	1	0	1	1	Ped: 1	Per: 4
1	5	1	2	0	0	0	2	0	2	1	1	0	0	1	Ped: 1	Per: 5
1	6	0	0	7	0	0	2	0	1	1	0	1	1	1	Ped: 1	Per: 6
1	7	3	6	14	8	8	1	0	2	1	0	1	0	1	Ped: 1	Per: 7
1	8	3	6	9	0	0	1	0	1	0	0	0	0	0	Ped: 1	Per: 8
1	9	8	10	0	0	0	2	0	2	1	0	0	1	1	Ped: 1	Per: 9
1	10	0	0	9	0	0	2	0	1	1	1	0	1	1	Ped: 1	Per: 10
1	11	0	0	14	0	0	2	0	1	0	0	0	0	0	Ped: 1	Per: 11
1	12	0	0	15	0	0	2	0	1	1	1	0	1	1	Ped: 1	Per: 12
1	13	0	0	16	0	0	1	0	1	0	0	0	0	0	Ped: 1	Per: 13
1	14	7	11	0	15	0	2	0	1	1	0	1	1	1	Ped: 1	Per: 14
1	15	7	12	0	0	16	2	0	1	1	0	1	1	1	Ped: 1	Per: 15
1	16	13	12	0	0	0	2	0	1	1	0	0	1	1	Ped: 1	Per: 16
.TE
 
The following data refer to a larger pedigree, taken from a coronary
heart disease study, in PEDFILE form:
.TS
center;
n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 n2 l l.
1	1	0	0	3	0	0	2	0	2	3	0	0	0.00	Ped: 1	Per: 1
1	2	0	0	3	0	0	1	1	2	3	0	0	0.00	Ped: 1	Per: 2
1	3	2	1	7	5	5	1	0	2	2	0	0	0.00	Ped: 1	Per: 3
1	4	0	0	7	0	0	2	0	1	2	0	0	0.00	Ped: 1	Per: 4
1	5	2	1	21	0	0	2	0	1	3	0	1	22.70	Ped: 1	Per: 5
1	6	0	0	21	0	0	1	0	2	3	0	0	0.00	Ped: 1	Per: 6
1	7	3	4	26	9	9	1	0	2	2	0	0	0.00	Ped: 1	Per: 7
1	8	0	0	26	0	0	2	0	1	2	0	1	9.20	Ped: 1	Per: 8
1	9	3	4	31	11	11	1	0	2	2	1	1	24.30	Ped: 1	Per: 9
1	10	0	0	31	0	0	2	0	1	2	1	0	9.30	Ped: 1	Per: 10
1	11	3	4	0	12	12	1	0	2	2	1	1	23.90	Ped: 1	Per: 11
1	12	3	4	34	14	14	1	0	2	2	1	1	20.70	Ped: 1	Per: 12
1	13	0	0	34	0	0	2	0	1	2	1	0	14.50	Ped: 1	Per: 13
1	14	3	4	0	15	15	2	0	1	2	1	0	2.10	Ped: 1	Per: 14
1	15	3	4	40	17	17	1	0	2	2	0	0	0.00	Ped: 1	Per: 15
1	16	0	0	40	0	0	2	0	1	2	1	1	9.80	Ped: 1	Per: 16
1	17	3	4	43	19	19	1	0	2	2	0	0	0.00	Ped: 1	Per: 17
1	18	0	0	43	0	0	2	0	1	2	1	0	11.50	Ped: 1	Per: 18
1	19	3	4	0	0	0	1	0	1	2	1	0	9.20	Ped: 1	Per: 19
1	20	0	0	47	0	0	2	0	0	1	0	0	0.00	Ped: 1	Per: 20
1	21	6	5	47	22	22	1	0	2	2	0	0	0.00	Ped: 1	Per: 21
1	22	6	5	48	24	24	1	0	2	2	0	0	0.00	Ped: 1	Per: 22
1	23	0	0	48	0	0	2	0	1	2	1	0	13.40	Ped: 1	Per: 23
1	24	6	5	0	25	25	2	0	1	2	1	1	10.40	Ped: 1	Per: 24
1	25	6	5	0	0	0	2	0	1	2	1	1	9.90	Ped: 1	Per: 25
1	26	7	8	0	27	27	2	0	1	2	1	1	16.80	Ped: 1	Per: 26
1	27	7	8	53	29	29	2	0	1	2	0	1	30.10	Ped: 1	Per: 27
1	28	0	0	53	0	0	1	0	1	2	1	0	6.90	Ped: 1	Per: 28
1	29	7	8	56	0	0	2	0	1	2	1	1	15.40	Ped: 1	Per: 29
1	30	0	0	56	0	0	1	0	1	2	1	0	14.30	Ped: 1	Per: 30
1	31	9	10	0	32	32	2	0	1	1	1	0	6.80	Ped: 1	Per: 31
1	32	9	10	0	33	33	1	0	1	1	1	0	5.60	Ped: 1	Per: 32
1	33	9	10	0	0	0	2	0	1	1	1	1	31.60	Ped: 1	Per: 33
1	34	12	13	0	35	35	1	0	1	1	1	0	19.40	Ped: 1	Per: 34
1	35	12	13	0	36	36	2	0	1	1	1	1	41.70	Ped: 1	Per: 35
1	36	12	13	0	37	37	1	0	1	1	1	0	20.50	Ped: 1	Per: 36
1	37	12	13	0	38	38	1	0	1	1	1	1	28.40	Ped: 1	Per: 37
1	38	12	13	0	39	39	2	0	1	1	1	0	11.50	Ped: 1	Per: 38
1	39	12	13	0	0	0	2	0	1	1	1	0	21.00	Ped: 1	Per: 39
1	40	15	16	0	41	41	2	0	1	1	1	0	10.50	Ped: 1	Per: 40
1	41	15	16	0	0	0	2	0	1	1	1	0	12.60	Ped: 1	Per: 41
1	42	0	0	52	0	0	1	0	1	1	1	0	11.20	Ped: 1	Per: 42
1	43	17	18	52	44	44	2	0	1	1	1	1	37.20	Ped: 1	Per: 43
1	44	17	18	0	45	45	2	0	1	1	1	0	10.10	Ped: 1	Per: 44
1	45	17	18	0	46	46	1	0	1	1	1	1	34.90	Ped: 1	Per: 45
1	46	17	18	0	0	0	1	0	1	1	1	1	25.30	Ped: 1	Per: 46
1	47	21	20	0	0	0	2	0	1	1	1	1	47.90	Ped: 1	Per: 47
1	48	22	23	0	50	50	2	0	1	1	1	0	14.00	Ped: 1	Per: 48
1	49	0	0	51	0	0	1	0	0	1	0	0	0.00	Ped: 1	Per: 49
1	50	22	23	51	0	0	2	0	1	2	1	1	55.30	Ped: 1	Per: 50
1	51	49	50	0	0	0	2	0	1	1	1	0	13.60	Ped: 1	Per: 51
1	52	42	43	0	0	0	2	0	1	1	1	0	12.50	Ped: 1	Per: 52
1	53	28	27	0	54	54	1	0	1	1	1	1	37.50	Ped: 1	Per: 53
1	54	28	27	0	55	55	1	0	1	1	1	1	14.70	Ped: 1	Per: 54
1	55	28	27	0	0	0	2	0	1	1	1	1	29.90	Ped: 1	Per: 55
1	56	30	29	0	57	57	1	0	1	1	1	0	5.70	Ped: 1	Per: 56
1	57	30	29	0	0	0	2	0	1	1	1	0	8.20	Ped: 1	Per: 57
.TE
 
Here, three loci are represented. The first is an affection status
locus (coronary disease symptoms) with three liability classes (ages
0-20, 20-40, and greater than 40); the second is a binary factor locus
(LDL receptor polymorphism), and the third is a quantitative variable
(age adjusted LDL cholesterol levels). Individuals with unknown
affection status are assigned to the first liability class in this
example, but the results would be the same irrespective of this
assignment.
.SH NOTES
The information contained herein was gleaned, often-times verbatim,
from 
.UR http://linkage.rockefeller.edu/soft/linkage/
the LINKAGE User's Guide
.UE
on the web by kind permission of Jurg Ott, Ph.D.

.SH AUTHORS
Mark Lathrop and Jurg Ott.
.PP
This manual page was written by Elizabeth Barham
<lizzy@soggytrousers.net> for the Debian GNU/Linux distribution.

.SH WORLD-WIDE-WEB
.UR http://linkage.rockefeller.edu/soft/linkage/
http://linkage.rockefeller.edu/soft/linkage/
.UE

.SH SEE ALSO
.BR LINKAGE (5),
.BR DATAFILE (5),
.BR ilink (1),
.BR linkmap (1),
.BR lodscore (1),
.BR mlink (1),
and
.BR unknown (1).
