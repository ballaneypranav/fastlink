Source: fastlink
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Charles Plessy <plessy@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 11~)
Standards-Version: 4.1.4
Vcs-Browser: https://salsa.debian.org/med-team/fastlink
Vcs-Git: https://salsa.debian.org/med-team/fastlink.git
Homepage: https://www.ncbi.nlm.nih.gov/CBBResearch/Schaffer/fastlink.html

Package: fastlink
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: fastlink-doc
Description: faster version of pedigree programs of Linkage
 Genetic linkage analysis is a statistical technique used to map
 genes and find the approximate location of disease genes. There
 was a standard software package for genetic linkage called
 LINKAGE. FASTLINK is a significantly modified and improved
 version of the main programs of LINKAGE that runs much faster
 sequentially, can run in parallel, allows the user to recover
 gracefully from a computer crash, and provides abundant new
 documentation. FASTLINK has been used in over 1000 published
 genetic linkage studies.
 .
 This package contains the following programs:
  ilink:    GEMINI optimization procedure to find a locally
            optimal value of the theta vector of recombination
            fractions
  linkmap:  calculates location scores of one locus against a
            fixed map of other loci
  lodscore: compares likelihoods at locally optimal theta
  mlink:    calculates lod scores and risk with two of more loci
  unknown:  identify possible genotypes for unknowns

Package: fastlink-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: fastlink
Description: Some papers about fastlink
 Genetic linkage analysis is a statistical technique used to map
 genes and find the approximate location of disease genes. There
 was a standard software package for genetic linkage called
 LINKAGE. FASTLINK is a significantly modified and improved
 version of the main programs of LINKAGE that runs much faster
 sequentially, can run in parallel, allows the user to recover
 gracefully from a computer crash, and provides abundant new
 documentation. FASTLINK has been used in over 1000 published
 genetic linkage studies.
 .
 You do not really need these papers about fastlink but it is highly
 recommended to study this documentation before starting with the
 tools of the fastlink package.
