From: ftp-bimas.cit.nih.gov                       Last mod: August 15, 2000

	DJGPP Installation and Use Instructions
       


This file describes how to retrieve, install, and use the djgpp C
compiler for DOS. djgpp is a port of gcc to DOS done by
DJ Delorie. It is freely available via ftp over the Internet. djgpp
is the compiler we use to make the executable files for FASTLINK for
DOS.  Big thanks to Ramana Idury and Roger Kou who first ported
FASTLINK to DOS. Thanks to Toby Nygaard for updating this file
to reflect changes in djgpp.

The djgpp compiler understands make.  The source code for FASTLINK for
DOS is *exactly* the same as the source code for the UNIX versions of
FASTLINK. All you need to do to prepare to compile the code for DOS is
add the flag -DDOS by changing the line in the Makefile that currently
reads:

  #SYSDEP =

to:

  SYSDEP = -DDOS

A significant incentive for you to install djgpp and compile FASTLINK
for yourselves is that you will get to try the "fast" version of
FASTLINK for 2 and 3 point runs. The executables we distribute are for
the "slow" version of FASTLINK, which is still measurably faster than
LINKAGE.

djgpp is configured to cope with the fact that the Makefile uses long
(longer than 8 characters before the dot) file names, but DOS
traditionally supports 8.3 file names. On newer systems that support
longer file names, you either have to modify the file names so that
they are truncated to the 8.3 limit.

See http://www.delorie.com/djgpp/
for more up to date information.

|*| File Retrieval for setting up the djgpp C compiler. 

This is mostly about retrieving the files.  ftp is file transfer
protocol. It is a program for transferring files from one computer to
another via internet.  There are several ftp repositories on the
internet that have djgpp.  One is at Oakland University in Michigan
called - oak.oakland.edu

Start at the prompt in the drive you wish to install djgpp in e.g.,
[Drive letter]:\>
You need to make a directory to put the compiler in,
such as, 
[Drive letter]:\djgpp (the compiler name is djgpp).  
To do this 
Type "md djgpp" (here and in all cases do not type the "")

Retrieving files from the ftp repository. 
       ftp ftp.simtel.net

You should get a login prompt. Although you do not have an account
with this computer you can log in as anonymous. (Many computers on the
net allow access to the general public this way)
type - "anonymous"
At the password prompt, type in your full e-mail address.

The files are currently located in the directory SimTel/vendors/djgpp
- to get there type
   cd pub/simtelnet/gnu/djgpp

This directory contains a large number of files that take a long time
to retrieve all together.

As of July 24, 1995, the recommended files necessary are:
bnu252.zip (The GNU assembler, previously in gasXXXbn.zip, is now
included in this file)
djdev112.zip
djdocXXX.zip
djeoe112.zip
djgpp.faq
dj112m1.zip  (see note below)
dj112m2.zip  
dj112m3.zip  
dj112m4.zip  
gcc263bn.zip
mak371bn.zip
split.exe
unzip386.exe

More up to date instructions can be found via the Web page listed above.

The dj112mX.zip files are updates.  When you install the compiler, it
will be important to unzip and install (by running the 112mX.bat file
in each dj112mX.zip file) each update sequentially.

To retrieve these files use the get command (before using you must set
to binary transfer rather then ascii), do this by typing "bin".

Optional; type "hash" (this lets you know how transfer is proceeding)
       type "bell" (this tells you when each operation is done)
For each file you want. type
       "get filename" (ex: get split.exe)
When done type "quit" to exit archive.

As you can see most of the files you retrieve are zip files.  You
should use the program unzip386.exe which you will have retrieved to
unzip them.  Different versions of unzip have been unsuccessful at
unzipping these files.  To unzip files type unzip386 filename.zip (you
only need to unzip .zip files)
   ex:unzip386 djeoe112.zip

|*| Installing djgpp and compiling FASTLINK


To install djgpp, find the file readme.dj.  Follow the directions in
readme.dj verbatim.  You must be extremely careful to type the
modifications to your system files exactly as they are shown there,
particularly with the exact same spaces or lack of spaces.

(1) Execute the following command before any compilation. 
  setdjgpp [Drive letter]:\djgpp [Drive letter]:/djgpp
where [Drive letter]:\djgpp is the directory where DJGPP has been
installed. If it is installed at a different location change the above
command accordingly.  This step is needed since DJGPP works and
behaves like a Unix compiler.  As such the Makefile from the Unix
version can be used directly.

(2) Change the working directory to wherever the fastlink source code
files are located.

(3) Run
  make installslow -f makefile
This will create the files ilink. lodscore, linkmap, and mlink.

(4) If you are using a version 1.XX of djgpp, then
    at this stage the files are not exactly suitable 
    for execution on a DOS machine. You need to run

   coff2exe -s [Drive letter]:\djgpp\bin\go32.exe ilink lodscore mlink linkmap

The program does not seem to find go32.exe without stipulating the
path even if its path is included in one's autoexec.bat file.

If you are using a newer version 2.XX of djgpp, then the programs you
compiled (unknown mlink ilink lodscore linkmap) are executable, but
you may need to rename them with a .exe extension, so that they can be
integrated with the LINKAGE auxiliary programs.

If the compilation cannot find certain library functions it is either because:
1. You did not modify the system configuration files exactly right or

2. You did not unzip the djdev file in a subdirectory which is on the
path where djgpp can find it.


