From: ftp://ftp.ncbi.nih.gov/pub/fastlink          Last mod: March 31, 2012

             Portability Information

With the assistance of many users, I have investigated the portability
of FASTLINK to the following operating systems. In this file I
summarize the results. In those cases where there is something
interesting to be said, I refer to another README file with more
details. In general, some more portability information is badly
needed, especially for the LINKAGE auxiliary programs (preplink,
makeped, lcp, lrp, lsp, unknown). User assistance with portability
tests would be greatly appreciated!


|*| AIX --- the standard operating system for IBVM RS6000 workstations.
      Code in the main programs seems to port fine. Compilation requires
      some minor changes to the Makefile. Of the auxiliary programs,
      I could only get lsp and unknown to compile and work.
      See README.AIX for more details.

|*| Digital UNIX --- operating system for DEC Alpha workstations.
      Earlier versions of this operating system are called OSF.
      The code compiles fine. While running it may hit a floating
      point exception due to a bug in some C compilers. See
      README.Digital for more details. Auxiliary programs are available
      at ftp.well.ox.ac.uk (maintained by Dan Weeks)


|*| DOS/Windows --- the standard operating system for IBM-compatible PC's
     Ramana Idury has done a successful port to DOS. He used the
     dgjpp compiler, which is a freely-available port of the
     gcc compiler. He makes no guarantees about what happens
     with any other compiler. Auxiliary programs for DOS
     are available through Jurg Ott. See README.DOS for
     more information. See README.djgpp for how to obtain, install,
     and use the djgpp compiler yourself.

     In 2006, I compiled new executables for Windows and these are
     posted in the pub/fastlink/windows subdirectory, which is
     equivalent to the URL:
     ftp://ftp.ncbi.nih.gov/pub/fastlink/windows

|*| HP/UX -- the standard operating system for some Hewlett-Packard 
             workstations.
     Tara Cox Matise reports that the main programs port fine.
     Auxiliary programs for HP/UX are available from the Utah FTP site
     through gopher 128.110.231.1 (maintained by Dean Flanders); look
     under publically available software.

|*| IRIX --- the standard operating system for some SGI workstations.
     David Featherstone reports that the code in the main programs
     seems to port fine. Compilation requires some minor changes to the
     Makefile. Auxiliary programs are available from John Powell 
     (jip@helix.nih.gov) or Jim Tomlin (jtomlin@helix.nih.gov)

|*| Linux --- freely available version of UNIX for PC's 
      Xiali Xie, Shriram Krishnamurthy, and Kurt Hornik report that
      the main programs port fine. Among the auxiliary programs,
      unknown and lsp port fine. The others require nontrivial changes.
      See README.Linux.

|*| Mac OSX -- executables compiled by David Stockton are in the
      pub/fastlink/mac directory as one zip file.
      Equiavlently, you can retrive them with the URL:
      ftp://ftp.ncbi.nlm.nih.gov/pub/fastlink/mac/FASTLINK_executables.zip

|*| OS/2  --- operating system for some IBM-compatible PC's
      Luc Krols reports that he was able to get an earlier version of
      FASTLINK to compile with a commercially available C compiler.
      I would appreciate if users with OS/2 machines could test
      more recent versions. Auxiliary programs for OS/2 are available
      from Jurg Ott.

|*| Solaris 2.* --- operating system for newer Sun workstations.
      The code ports fine. 
      Auxiliary programs for Solaris are available from the Utah FTP site
      through gopher 128.110.231.1 (maintained by Dean Flanders); look
      under publically available software.


|*| SunOS --- operating system for older Sun workstations.
      The code ports fine.
      Auxiliary programs for SunOS are available from the Utah FTP site
      through gopher 128.110.231.1 (maintained by Dean Flanders); look
      under publically available software.

|*| Ultrix --- operating system for some DEC workstations.
      The code ports fine. 
      Auxiliary programs for Ultrix are available from the Utah FTP site
      through gopher 128.110.231.1 (maintained by Dean Flanders); look
      under publically available software.

|*| VMS  --- operating system for some DEC workstations.
      There are versions of VMS for VAXStations and Alphas.
      What I know is largely based on reports from Kimmo Kallio, who
      uses VMS on an Alpha, Steve Roberts who uses VMS on a VAXStation,
      Shriram Krishnamurthi who help me with the VAX port, and Don
      Plugge who helped me test FASTLINK 4.0P on AlphaVMS.
      The code can be ported after some changes to the filenames 
      used in checkpointing and a few changes to code files.
      Auxiliary programs for VMS are available from the Utah FTP site
      through gopher 128.110.231.1 (maintained by Dean Flanders); look
      under publically available software.
      See README.VMS for more details.
