From: ftp-bimas.cit.nih.gov                       Last mod: June 27, 1999

                  FASTLINK, version 4.1P

This file describes using the Makefile included in this distribution to
build FASTLINK, version 4.1P. See README for an overview of all the
documentation.

FTP instructions, source code organization, and an overview of compilation
is described in README.install.

README.constants explains the meaning of some constants that you will need
to set in the Makefile for compilation.

If you plan to run parallel FASTLINK, please also refer to README.parallel,
and either README.p4 (for shared-memory), or README.TreadMarks (for
networks) to fully install the parallel version.


|*| Makefile, Introduction
    ----------------------

There file Makefile residing in the 3.0P/src directory is used by the
make utility to determine how to build FASTLINK.  They are:

The Makefile is broken into two main sections.  The first includes the
definition of parameters, while the second describes dependencies among
source files, compilation rules, and targets.  You will only need to be
concerned with the first section.


|*| Makefile, Variables
    -------------------

Parameters, or variables for compilation are defined in the Makefile with
the syntax:

  <VAR> = <VAL>

where variable <VAR> is assigned the value <VAL>.  For example, to define
the variable CC (denoting the name of the C compiler) to be gcc, the
Makefile would include:

  CC = gcc

Variables can be referred to by surrounding the variable name in a $(...)
enclosure.  For example, given the definition above, the statement:

  $(CC) file.c

would be interpreted by make as:

  gcc file.c

Your editing of the Makefile will consist of choosing, or defining
variables to suit your particular needs.  For example, if you were
instructed to set the variable CC to the compiler you want to use, and you
wanted to use cc instead of gcc, you would change the above definition to:

  CC = cc


|*| Makefile, Comments
    ------------------

In a Makefile, the pound symbol "#" denotes a comment.  Any line that
begins with a "#" is a comment included to make the file more readable, or
to explain the meaning of a variable.  These lines are ignored by make, and
have no effect on the compilation.  

In addition to defining variables as described above, you will in places be
instructed to either "comment out", or to "uncomment" a line.

"Uncommenting" a line has the effect of making a line visible to make that
it previously could not see (because make ignores comments).  For example,
if you are running Sun's Solaris operating system, you will be instructed
to uncomment the definition of the SYSDEP variable.  This means that you
need to erase the "#" character from the beginning of the line.  The line:

  #SYSDEP = -DSOLARIS

should be changed to:

  SYSDEP = -DSOLARIS

Conversely, "commenting out" a line has the effect of telling make to
ignore the line. For example, if you don't want to use compiler
optimization, you would comment out the line:

  OPTFLAG = -O2

by adding a "#" to the beginning of the line, so it reads:

  #OPTFLAG = -O2

Note that when there are two possible definitions of a variable, you should
make sure that no more than one are uncommented.  This can lead to
unexpected problems.


|*| FASTLINK, Memory Usage Options
    ------------------------------

One of the issues you will be asked to consider when editing the Makefile
is the level of memory FASTLINK can use.  There are two variables that
combine to give three different levels of possible memory usage.  The
variables are:

LESSMEMORY
  If defined (using the -D flag described in README.install), LESSMEMORY
  instructs FASTLINK to use an alternate algorithm which requires
  considerably less memory, but is slower.  The file README.memory
  discusses the differences.

PRECOMPUTE
  This variable is defined as either 1, 0, where PRECOMPUTE=1 instructs
  FASTLINK to use an alternate algorithm which requires more memory, and
  is faster.  PRECOMPUTE=0 uses the normal approach.

The possible combinations of these variables is as follows:

Lowest Memory Used -- Slowest
  LESSMEMORY defined and PRECOMPUTE=0

Middle Level -- Medium Speed, available only for parallel version
  LESSMEMORY not defined and PRECOMPUTE=0

Highest Level -- Fastest
  LESSMEMORY not defined and PRECOMPUTE=1

You should always be able to use the fastest level for 2 and 3 locus analysis.
Depending on how much memory you have, you may need to go down to the
Middle Level or Lowest Level for 4 point analysis. You should use the
Lowest Level for 5 or more loci.

|*| Makefile, Editing
    -----------------

Editing the Makefile amounts to simply answering a number of
questions.  Much of the following information appears within comments
in the Makefile itself to simplify the editing process.  All of the
relevant information occurs in the first 200 lines or so of the
Makefile; this corresponds to roughly 25 decisions.  Most of these
decisions are relevant only to the parallel version.

Start at the beginning of the Makefile, and work through the variables one
at a time until you reach the line:

  # You shouldn't need to change anything beyond this point. #

The variables are grouped together in logical sections.  The following is
an explanation of each of the variables, section by section, including
their definitions as they appear in the Makefile.


|*| Makefile, Miscellaneous Variables
    ---------------------------------

The following variables apply to compilation of *both* sequential *and*
parallel FASTLINK

BINDIR = ./bin
  This is the destination directory where you want your executables
  installed.  Note: this directory *must* exist, or compilation will
  fail during linkage.

OPTFLAG = -O
  Compiler optimization flag.  Comment this out if your compiler complains.

PROTO = -DKNR_PROTO
  If you use the cc compiler, uncomment this flag to use K&R (Kernighan and
  Ritchie) style function prototypes.  Note: FASTLINK will *not* compile
  under cc without this flag set.

SYSDEP = -DSUNOS_4_1_X_GCC
SYSDEP = -DSOLARIS
SYSDEP = -D_POSIX_SOURCE
SYSDEP =
  All of these variables are commented out by default. 
  - Uncomment the -DSOLARIS option if you are compiling on a Sun Solaris 
    platform.
  - If you are using gcc under SunOS 4.1.x, you can use -DSUNOS_4_1_X_GCC
    to get rid of compiler warnings if they appear.
  - Uncomment -D_POSIX_SOURCE if you are compiling for AIX.
  - If you have other system-specific compiler flags that you
    need to pass to FASTLINK, uncomment the line "SYSDEP =",
    and add the flag there.
  Otherwise, leave this alone.


|*| Makefile, Sequential Code Variables
    -----------------------------------

The following variables apply to sequential FASTLINK only.

CC = gcc
  This defines the C compiler for sequential code.

CKPT = -DDOS
  This variable determines whether or not to include checkpointing code in
  FASTLINK.  Uncomment this if you are compiling for DOS, or if you want to
  turn off checkpointing in FASTLINK.


|*| Makefile, Parallel Code Variables
    ---------------------------------

These variables apply only to parallel FASTLINK.
You do not need to touch any of this stuff if you are running sequentially.

PARCC = gcc
  C compiler for parallel code.

PARLIB = -DIS_P4=1
PARLIB = -DIS_PARMACS=1
  Parallel support library. If you are using TreadMarks, leave these
  commented out. If you are using P4 or PARMACS, uncomment the appropriate
  variable.  Make sure that no more than one of these variables is
  uncommented!  

PARINCLPATH = -I$(TmkDIR)/include -I/usr/local/include
PARINCLPATH = -I$(P4_INCLDIR)
  Parallel include path. If you are using TreadMarks or P4, uncomment the
  appropriate variable.  If you are using PARMACS, comment these out, or
  supply your own, appropriate path. Make sure that no more than one of
  these variables is uncommented!


|*| Makefile, Parallel Code Variables, TreadMarks specifics
    -------------------------------------------------------

These variables apply only to users of TreadMarks.  Please refer to the
README.TreadMarks for more details.

TmkDIR = /usr/lib/Tmk-0.9.3
  Set this to the root directory of your TreadMarks installation

ARCH = sparc
  Target architecture.  For example, if your TreadMarks library is in
  $(TmkDIR)/lib.mips, you should set this variable to mips.  Consult your
  TreadMarks documentation for more details.

ATMDIR = /usr/fore/lib
  If you are using TreadMarks on an ATM network, set this to the location
  of libatm.a on your system.


|*| Makefile, Parallel Code Variables, P4 specifics
    -----------------------------------------------

These variables apply only to users of P4.  Please refer to the README.p4
for more details.

P4_HOME_DIR = /usr/lib/p4-1.4
  Set this to the root directory of your p4 installation.  Refer to the
  README.p4 for details.

P4_MDEP_LD = -lsocket -lnsl -lthread
  Uncomment this variable if you are running in parallel on SOLARIS. This
  is *very* important -- FASTLINK will not compile without it.


|*| Makefile, Parallel Code Variables, Parallel C Compiler Flags
    ------------------------------------------------------------

Edit the following variables when compiling parallel FASTLINK to suit
your particular run.

There are 3 sets of variables provided -- one each for ILINK, MLINK, and
LINKMAP.  The variables are **PARMEM, **PRECOMP, where **
represents one of IL, ML, or LI, for ILINK, MLINK and LINKMAP respectively.
They are briefly described below.  Please refer to the files
README.constants, README.memory, and README.parallel, as well as to the
section above on Memory Usage Options.

**PARMEM
   Defines the variable LESSMEMORY, instructing FASTLINK to use an
   alternate algorithm which requires less memory, but is slower.  Please
   refer to the README.memory, as well as to the section on Memory Usage
   Options above.  Uncomment this if you wish to use less memory.
   Note: setting this value to 0 *or* 1 will both have the same effect of
   defining LESSMEMORY.  If you don't want less memory, be sure to comment
   this out.
**PRECOMPUTE
   Defines the variable PRECOMPUTE as 1 or 0.  PRECOMPUTE=1 instructs
   FASTLINK to use an alternate algorithm which requires more memory, and
   is faster.  PRECOMPUTE=0 uses the normal approach.  Please refer to the
   README.memory, as well as to the section above on Memory Usage Options
   for more details.  Note: Unlike **PARMEM, this value *must* be set to
   either 0 or 1.


|*| Makefile, Unix Utilities on Your System
    ---------------------------------------

If any of the following utilities are different on your system, you can
change them here.  If not, there is nothing more to do.

RM = rm -f
  RM is used for "make clean", so if you want to use that, you'll need
  something defined here.


|*| Makefile, Passing Extra Flags
    -----------------------------

Most versions of make allows you to override Makefile variables from the
command line.  The Makefile includes a variable EXTRAFLAGS that is
intentionally left empty to allow you to add command-line flags without
editing the Makefile.  EXTRAFLAGS is automatically passed to each of the
different targets.
If you want to change a constant, such as maxloop, you would add a suitable
-D command to EXTRAFLAGS (see README.constants).

For example, if you wanted to pass along an additional include directive to
the compiler when making ILINK, you could type:

  make ilink EXTRAFLAGS="-I/usr/ucb/include -I/usr/lib/include"

Note that if you want to add more than one flag, you need to protect the
argument in double quotes (") as above.

